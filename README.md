_Código de Treinador:_ 5639 3038 8615  
_Telegram:_ https://t.me/Kyle_Felipe  
_Whatsapp_ 31-99308-4962  

# Pokémon para troca:  

## Quero: ##

*   Pikachu: Normal, Evento, Shine
*   Raichu: Normal, Evento, Shine
*   Bulbasauro: Normal, Evento, Shine
*   Squirtle: Normal, Evento, Shine
*   Chansey
*   Blissey
*   Phanpy
*   Larvitar
*   Raltz
*   Geodude
*   Trekko
*   Torchick
*   Bagon
*   Mudkip
*   Makuhita: Normal, Shine
*   Hariyama: Normal, Shine
*   Machamp
*   Aron: Shine
*   Beldum
*   Meditite
*   Anorith
*   Magikarp: Normal, Shine
*   Rayquaza
*   Hitmonchan
*   Hitmonle
*   Registeel
*   Latias
*   Latios
*   Kyogre
*   Jirachi
*   Deoxys
*   Minun: Shine
*   Plusle: Shine
*   Spinda
*   Lunatone
*   Lileep
*   Toggepi: Normal, Shine
*   Beldum

## Tenho: ##
    QT Nome
*   1 Pichu
*   2 Bulbasauro
*   2 Ivysauro
*   3 Charmander
*   1 Chameleon
*   2 Squirtle
*   1 Blastoise
*   1 Metapod
*   3 Beedrill
*   1 Pidgey
*   1 Pidgeotto
*   2 Pidgeot
*   3 Rattata: Normal, 1 Alola
*   1 Spearow
*   1 Fearow
*   1 Arbok
*   3 Pikachu: Evento
*   5 Sandshrew: Normal, 2 Alola
*   2 Nidoran Femea
*   1 Nidoqueen
*   1 Nidoran Macho
*   1 Nidorino
*   1 Clefary
*   2 Vulpix
*   1 Nintales
*   2 Jigglypuf
*   3 Oddish
*   2 Paras
*   1 Parasect
*   2 Venonat
*   2 Venomoth
*   5 Diglett: Normal, 2 Alola
*   1 Dugtrio
*   2 Meowth: Normal, 1 Alola
*   2 Psyduck
*   1 Mankey
*   2 Primeape
*   1 Growlithe
*   2 Poliwhirl
*   3 Abra
*   1 Machop
*   2 Machoke
*   2 Bellsprout
*   1 Weepinbell
*   3 Tentacool
*   4 Geodude: Normal, 1 Alola 
*   1 Graveller
*   3 Ponyta
*   2 Rapidash
*   2 Slowpoke
*   1 Doduo
*   1 Seel
*   2 Grimer: Alola
*   1 Shelder
*   2 Cloister
*   1 Gastly
*   3 Onix
*   1 Krabby
*   2 Kingler
*   3 Voltorb
*   3 Exeggcute
*   3 Exeggcutor: Normal, 2 Alola
*   2 Cubone
*   1 Lickitung
*   1 Koffing
*   2 Hyhorn
*   1 Tangela
*   3 Horsea
*   3 Goldeen
*   1 Seaking
*   2 Staryu
*   1 Starmie
*   2 Scyther
*   2 Jynx
*   1 Electabuzz
*   3 Magmar
*   1 Pinsir
*   1 Magikarp
*   1 Lapras
*   2 Ditto
*   4 Eevee
*   1 Vaporeon
*   1 Jolteon
*   2 Flareon
*   1 Omanyte
*   2 Omastar
*   2 Kabuto
*   1 Aerodactyl
*   1 Snorlax
*   1 Zapdos
*   5 Chikorita
*   1 Meganium
*   2 Cyndaquil
*   1 Quilava
*   2 Totodile
*   1 Croconaw
*   1 Sentret
*   1 Furret
*   3 Hoothoot
*   2 ledyba
*   1 Ledian
*   3 Chinchou
*   1 Lantun
*   2 Togepi
*   2 Natu
*   1 Xatu
*   1 Mareep
*   1 Belossom
*   2 Sudowoodo
*   1 Hoppip
*   2 Jumpluff
*   1 Aipon
*   2 Sunkern
*   1 Yanma
*   1 wooper
*   2 Quagsire
*   1 Umbreon
*   1 Murkrow
*   3 Misdreavu
*   1 Wobbufet
*   1 Pineco
*   2 Gligar
*   2 Snubbull
*   1 Grandbull
*   1 Quilfish
*   13 Heracross
*   7 Teddiursa
*   3 Ursaring
*   3 Slugma
*   1 Magcargo
*   1 Swindub
*   3 Remoraid
*   1 Octillery
*   1 Mantine
*   1 Skarmory
*   2 Houndour
*   2 Phanpy
*   1 Stantler
*   3 Larvitar
*   3 Treecko
*   1 Sceptile
*   2 Torchic
*   1 Blaziken
*   2 Mudkip
*   1 Marstomp
*   2 Poochyena
*   1 Mightyena
*   2 Ziggzagoon
*   2 Linoone
*   1 Wurmple
*   2 Silcoon
*   2 Lotad
*   1 Nuzleaf
*   3 Taillow
*   1 Swellow
*   1 Wingull
*   3 Raltz
*   1 Kirlia
*   2 Surskit
*   1 Masterain
*   2 Shroomish
*   1 Blreloom
*   1 Slakoth
*   5 Whismur
*   3 Exploud
*   1 Makuhita
*   4 Hariyama
*   6 Nosepass
*   3 Skitty
*   1 Delcatty
*   1 Sableye
*   2 Mawille
*   11 Aron
*   1 Lairon
*   5 Aggron
*   1 Meditite
*   1 Medicam
*   1 Electrike
*   1 Plusle
*   1 Minum
*   5 Illumise
*   4 Roselia
*   1 Swalot
*   3 Carvanha
*   1 Sharpedo
*   3 Numel
*   1 Camelput
*   2 Spoink
*   1 Trapinch
*   1 Vibrava
*   2 Cacnea
*   8 Swablu
*   2 Serviper
*   1 Lunatone
*   3 Baroach
*   1 Whiscasch
*   3 Corpish
*   3 Baltoy
*   1 Claydol
*   2 Lileep
*   1 Cradily
*   3 Anorith
*   1 Feebas
*   8 Castform: Normal, 4 Chuva, 3 Noite
*   3 Shuppet
*   1 Banette
*   1 Duskull
*   1 Chimecho
*   1 Wynaut
*   2 spheal
*   1 Walrein
*   1 Luvdisc
*   1 Bagon